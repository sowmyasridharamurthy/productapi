**About**

This repo hosts few API tests using Supertest, which is based on Nodejs. 

The test coverage is not extensive by nature, however its a good starting point to get the initial feel of interacting with API's. 

The test framework applied is MOCHA and assertion library is CHAI. 



**Pre-Requisite**

a) Docker should be installed on local system and it should be running

b) The Above test framework need nodeJs, follow- https://nodejs.org/en/


**How to Execute the test** 

Step 1: Create a local directory and move inside the directory/folder

Step 2: Clone the repository to your local system  "git clone <repo_address>"

Step 3: Move into the repo folder "products-docker-composer"

step 4: In the terminal execute npm i /npm install 

step 5: In the left hand pane choose docker-compose.yml file and right-click to choose "compose up"

          or
          
        in the terminal execute the command "npm run dockerup"
        
step 6: Open another terminal, execute the command "npm test"





**Docker compose to run the products services all in one**

It runs a mongo db, product service and price engine after running the following command.

`docker-compose up`


