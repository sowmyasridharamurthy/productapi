import supertest from 'supertest';
import {expect } from 'chai';
const request = supertest('http://localhost:3001')

describe("Add New products", (done)=>{

    const updatePayload = {
        "name": "Updated name of the product ",
        "description": "Updated Description for"
    } 

    let id = (Math.random() + 1).toString(36).substring(4);

    const payload = {
        "id": id,
        "name": "name of the product-- " + id,
        "description": "Description for --" + id,
    }

    it("Add a new product",()=> {
        request.post('/product')
       .set('accept', 'application/json')
       .set('Content-Type', 'application/json')
       .send(payload)
       .expect(200).then(res =>{
            res.body.data.forEach(data => {
            expect(data.id).to.be.eq(id)
            })
       
       })
       
      
        request.put('/product/'+id)
               .send(updatePayload)
               .expect(200)
               .then(res => {
                   expect(res.statusCode).to.be.eq(200)
           })
      
    })

})