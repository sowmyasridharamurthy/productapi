import supertest from 'supertest';
import {expect } from 'chai';
const request = supertest('http://localhost:3001')

describe("Delete Scenarios", (done)=>{

    it("Delete a valid product",()=> {
       //Get available ID and delete one entry
       var randomEntry , totalLength, deleteEntry
       request.get('/product')
               .then(res => {
                    totalLength = res.body.length
                    randomEntry = Math.floor(Math.random() * totalLength);
                    deleteEntry = res.body[randomEntry].id
               })
               .then(()=> {
                if(deleteEntry == 'undefined'){
                    console.log("the ID is undefined")
               }else{
                request.delete('/product/'+ deleteEntry)
               .then(res =>{
                    expect(res.body.data).to.contain('{ok:1}')
                })
               }
               })
    })

    it("Try to delete invalid product",()=> {
        request.delete('/product/123456')
                  .then(res =>{
                      console.log(res.body)
                     expect(res.body.data).to.contain('{ok:0}')
                 })
     })


})