import supertest from 'supertest';
import {expect } from 'chai';

const request = supertest('http://localhost:3001')
const Ajv = require("ajv")
const ajv = new Ajv() 
const schema = {
    type: "object",
    properties: {
        currency: {type: "string"},
        price: {type: "number"} ,
        id: {type:"string"},
        name: {type: "string"},
        description: {type: "string"}
    }  , 
    required: ["id"],
    
}
const validate = ajv.compile(schema)

describe("Products service", ()=>{

    it("validates Schema",()=> {
        request.get('/product').end((err,res) =>{
           if(validate(res.body)){
            expect(validate(res.body)).to.be.true
           }else{
               console.log("ATTENTION !!! --The JSON schema for product seems to have an issue")
           }
            
        })
     })

    it("Fetches all the products",()=> {
       request.get('/product').then((res) =>{
           expect(res.body).to.not.be.empty
           expect(res.statusCode).to.be.eq(200)
           expect(res.type).to.be.eq('application/json')

           res.body.data.forEach(data => {
               expect(data.id).to.not.be.empty

           });
       })
        
    })


})
